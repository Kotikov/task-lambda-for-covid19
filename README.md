AWS lambda function implemented in Python. Every day displays information about the number of cases and recovered (covid-19) for a random country.

Environment Variables:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION




Terraform v0.13.5

Python v3.8

python.zip contains the libraries requests-2.24.0 beautifulsoup4-4.9.3 lxml-4.6.1 for the lambda function layer
