import requests
from bs4 import BeautifulSoup
from random import choice


def refined(string):
    return string.replace(',', '')


def get_html(url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) \
        AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
        'accept': '*/*'
    }
    response = requests.get(url, headers=headers)
    return response.text


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    random_row = choice(soup.find('table', id='main_table_countries_today').find('tbody').find_all('tr'))
    table_data = random_row.find_all('td')
    country = table_data[1].text.strip()
    total_cases = refined(table_data[2].text)
    total_recovered = refined(table_data[6].text)

    statistics = {'Country': country,
                  'Total Cases': total_cases,
                  'Total Recovered': total_recovered
                  }
    return statistics


def main(event, context):
    URL = 'https://www.worldometers.info/coronavirus/'
    return get_page_data(get_html(URL))
