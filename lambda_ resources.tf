locals {
  function_for_lambda = "outputs/covid_19.zip"
}


# data "archive_file" "zip" {
#  type = "zip"
#  source_file = "covid_19.py"
#  output_path = local.lambda_zip_location
#}


locals {
  libraries_for_lambda = "outputs/python.zip"
}

resource "aws_lambda_layer_version" "parsing_library" {
  filename = "outputs/python.zip"
  layer_name = "parsing_library"
  description = "requests-2.24.0 beautifulsoup4-4.9.3 lxml-4.6.1"
  source_code_hash = filebase64sha256(local.libraries_for_lambda)
  compatible_runtimes = [
    "python3.8"]

}

resource "aws_lambda_function" "lambda" {
  filename = local.function_for_lambda
  function_name = "covid_19"
  role = aws_iam_role.lambda_role.arn
  handler = "covid_19.main"
  source_code_hash = filebase64sha256(local.function_for_lambda)
  runtime = "python3.8"
  layers = [
    aws_lambda_layer_version.parsing_library.arn]
  timeout = 20
}

resource "aws_cloudwatch_event_rule" "every_day" {
  name = "every-day"
  description = "Fires every day"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "target-one-day" {
  arn = aws_lambda_function.lambda.arn
  rule = aws_cloudwatch_event_rule.every_day.name
  target_id = "One-Day"
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal = "events.amazonaws.com"
  statement_id = "AllowExecutionFromCloudWatch"
  source_arn = aws_cloudwatch_event_rule.every_day.arn
}

data "aws_lambda_invocation" "get_result" {
  function_name = aws_lambda_function.lambda.function_name
  input = file("configure-test-event/input.json")
  depends_on = [
    aws_lambda_function.lambda]
}